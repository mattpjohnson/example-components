import showdown from "https://cdn.jsdelivr.net/npm/showdown/dist/showdown.min.js";

export function MarkdownViewer({ children }) {
  var converter = new showdown.Converter();

  return <div>{converter.makeHtml(children)}</div>;
}
