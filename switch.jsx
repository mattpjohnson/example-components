import './style.scss'
import { Label } from './label.jsx'

export default function Switch({ children, value, valueSetter }) {
  return (
    <label className={`va-radio__option`}>
      <div className="clasp-switch">
        <input className="clasp-switch__input" type="checkbox" value={value} />
        <div className="clasp-switch__background" />
        <div className="clasp-switch__switch" />
      </div>
      <span className="va-radio__option__label-text">{children}</span>
    </label>
  )
}
