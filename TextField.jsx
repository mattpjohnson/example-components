import { FormField } from "./FormField.jsx";
import { Input } from "./Input.jsx";

export function TextField({ children }) {
  return (
    <FormField label={children}>
      <Input />
    </FormField>
  );
}
