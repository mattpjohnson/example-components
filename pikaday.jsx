import Pikaday from "https://cdn.jsdelivr.net/npm/pikaday/pikaday.js";
import "https://cdn.jsdelivr.net/npm/pikaday/css/pikaday.css";
import "./EntryField.scss";

let id = 1;

export default function PikadayDatepicker({}) {
  const myId = id++;
  const wrapper = (
    <div>
      <input type="text" className="entry-field" id={myId} />
    </div>
  );
  new Pikaday({ container: wrapper, field: wrapper.querySelector("input") });
  return wrapper;
}
