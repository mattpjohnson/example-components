import "./EntryField.scss";
import "./Input.scss";

export function Input() {
  return <input className="input entry-field" />;
}
