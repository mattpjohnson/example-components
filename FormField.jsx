import { Label } from "./label.jsx";

export function FormField({ label, children }) {
  return (
    <div className="form-field">
      <Label>{label}</Label>
      {children}
    </div>
  );
}
