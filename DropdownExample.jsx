import { DropdownField } from "./DropdownField.jsx";

export function DropdownExample() {
  return (
    <DropdownField
      children="Country"
      options={[
        { name: "United States" },
        { name: "Canada" },
        { name: "Spain" }
      ]}
      labelGetter={option => option.name}
    />
  );
}
