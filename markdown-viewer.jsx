import './markdown-viewer.scss'

export default function MarkdownViewer({ children }) {
  return (
    <div className="markdown-viewer">
      {new showdown.Converter().makeHtml(children)}
    </div>
  )
}
