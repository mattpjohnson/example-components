import { Dropdown } from "./Dropdown.jsx";
import { FormField } from "./FormField.jsx";

export function DropdownField({ children, ...props }) {
  return (
    <FormField label={children}>
      <Dropdown {...props} />
    </FormField>
  );
}
