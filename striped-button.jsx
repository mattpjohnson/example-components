import './btn-styles.css'

export default function StripedButton({ children, ...props }) {
  return (
    <button className="btn btn--stripe" {...props}>
      {children}
    </button>
  )
}
