import { flatpickr } from "https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.js";
import "https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css";
import "https://cdn.jsdelivr.net/npm/flatpickr/dist/themes/material_blue.css";
import "./EntryField.scss";
import "./Flatpickr.css";

let id = 1;

export function Flatpickr({}) {
  const myId = id++;
  const wrapper = (
    <div>
      <input className="entry-field" id={myId} />
    </div>
  );
  flatpickr(wrapper.querySelector("input"), { appendTo: wrapper });
  return wrapper;
}
