import { HoverPopover } from "./HoverPopover.jsx";

export function LinkPopover({ children, href, popoverBody }) {
  return (
    <HoverPopover
      children={<a href={href}>{children}</a>}
      popoverBody={popoverBody}
    />
  );
}
