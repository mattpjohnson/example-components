import './article-card.scss'

export default function ArticleCard({ title, image, date }) {
  return (
    <div className="article-card">
      <img className="article-card__image" src={image} />

      <h1>{title}</h1>
      <span className="article-card__date">{date}</span>
    </div>
  )
}
