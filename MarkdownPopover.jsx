import { HoverPopover } from "./HoverPopover.jsx";
import { MarkdownViewer } from "./MarkdownViewer.jsx";
import "./MarkdownPopover.scss";

export function MarkdownPopover({ children }) {
  return (
    <HoverPopover
      children={children}
      popoverBody={
        <div className="markdown-popover">
          <MarkdownViewer children={children} />
        </div>
      }
    />
  );
}
