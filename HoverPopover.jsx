import { MyPopper } from "./Popper.jsx";
import "./HoverPopover.scss";

export function HoverPopover({ children, popoverBody }) {
  const reference = <div>{children}</div>;
  const popper = <div className="hover-popover">{popoverBody}</div>;

  reference.addEventListener("mouseenter", () =>
    popper.classList.add("hover-popover--is-open")
  );

  reference.addEventListener("mouseleave", () =>
    popper.classList.remove("hover-popover--is-open")
  );

  return <MyPopper reference={reference} popper={popper} isopen={true} />;
}
