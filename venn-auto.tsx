import './venn-auto.scss'

export default function VennAuto({ children, ...props }: { children: string }) {
  return (
    <button className="va-button" {...props}>
      {children}
    </button>
  )
}
