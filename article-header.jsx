import './article-header.scss'

export default function ArticleHeader({ title, image, link }) {
  return (
    <div className="article-header">
      <div className="logo">Travels.com</div>
      <img src={image} />
      <h1>{title}</h1>
      {link && <a href={link}>Read More</a>}
    </div>
  )
}
