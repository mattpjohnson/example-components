import './smart-button.scss'

export default function SmartButton({ children, disabled = false }) {
  return <button className="smart-button-base smart-button">{children}</button>
}
