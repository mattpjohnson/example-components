export default function CustomComponent({ children, name, syntax }) {
  const url = `${name}.${syntax}`;
  const elementNameLookup = { [url]: name };

  importComponentsFrom(
    {
      sources: [
        {
          url,
          source: children[0].innerHTML,
        },
      ],
    },
    elementNameLookup
  );

  return <div />;
}
