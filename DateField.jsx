import { Flatpickr } from "./flatpickr.jsx";
import { FormField } from "./FormField.jsx";

export function DateField({ children }) {
  return (
    <FormField label={children}>
      <Flatpickr />
    </FormField>
  );
}
