import { LinkPopover } from "./LinkPopover.jsx";
import "./GitHubProfileInfoLink.scss";

export function GitHubProfileInfoLink({ children }) {
  const popoverBody = (
    <div className="middle-container container">
      <div className="profile block">
        <a className="add-button" href="#28">
          <span className="icon entypo-plus scnd-font-color" />
        </a>
        <div className="profile-picture big-profile-picture clear">
          <img
            width="150px"
            alt="Matt Johnson profile picture"
            src="https://avatars3.githubusercontent.com/u/7467053?s=460&v=4"
          />
        </div>
        <h1 className="user-name">Matt Johnson</h1>
        <div className="profile-description">
          <p className="scnd-font-color">
            Lorem ipsum dolor sit amet consectetuer adipiscing
          </p>
        </div>
        <ul className="profile-options horizontal-list">
          <li>
            <a className="comments" href="#40">
              <p>
                <span className="icon fontawesome-comment-alt scnd-font-color" />
                23
              </p>
            </a>
          </li>
          <li>
            <a className="views" href="#41">
              <p>
                <span className="icon fontawesome-eye-open scnd-font-color" />
                841
              </p>
            </a>
          </li>
          <li>
            <a className="likes" href="#42">
              <p>
                <span className="icon fontawesome-heart-empty scnd-font-color" />
                49
              </p>
            </a>
          </li>
        </ul>
      </div>
    </div>
  );

  return (
    <LinkPopover
      children={children}
      href={`https://github.com/${children}`}
      popoverBody={popoverBody}
    />
  );
}
