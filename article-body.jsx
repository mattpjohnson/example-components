import './article-body.scss'

export default function ArticleBody({ title, children }) {
  return (
    <div className="article-body">
      <div className="article-body__heading">
        <h1>{title}</h1>
        <h3>Personal blog</h3>
      </div>
      <div className="article-body__body">{children}</div>
    </div>
  )
}
