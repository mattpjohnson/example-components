import "https://cdnjs.cloudflare.com/ajax/libs/highlight.js/9.15.10/styles/monokai-sublime.min.css";
import "https://cdnjs.cloudflare.com/ajax/libs/highlight.js/9.15.10/languages/go.min.js";
import hljs from "https://cdnjs.cloudflare.com/ajax/libs/highlight.js/9.15.10/highlight.min.js";
import { HoverPopover } from "./HoverPopover.jsx";

export function CodeViewer({ language, children }) {
  const codeBlock = <code class={language}>{children}</code>;
  hljs.highlightBlock(codeBlock);
  return (
    <HoverPopover
      children={language + " code"}
      popoverBody={<pre>{codeBlock}</pre>}
    />
  );
}
