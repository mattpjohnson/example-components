import Choices from "https://cdn.jsdelivr.net/npm/choices.js";
import "https://cdn.jsdelivr.net/npm/choices.js/public/assets/styles/base.min.css";
import "https://cdn.jsdelivr.net/npm/choices.js/public/assets/styles/choices.min.css";
import "./Dropdown.scss";
import "./EntryField.scss";

export function Dropdown({
  options = [],
  labelGetter = option => option,
  valueGetter = option => option,
  onChange = Function
}) {
  const wrapper = (
    <div>
      <select
        onChange={event =>
          onChange(valueGetter(options[Number(event.target.value)]))
        }
        // value={options.findIndex(
        //   opt => labelGetter(opt) === labelGetter(selected || {})
        // )}
      >
        {options.map((opt, i) => (
          <option key={i} value={i}>
            {labelGetter(opt)}
          </option>
        ))}
      </select>
    </div>
  );

  const classNames = {
    containerOuter: "choices dropdown__outer",
    containerInner: "",
    item: "choices__item entry-field",
    itemSelectable: "",
    list: "choices__list dropdown__list",
    listSingle: "choices__list--single h-full"
  };

  new Choices(wrapper.querySelector("select"), { classNames });

  return wrapper;
}
