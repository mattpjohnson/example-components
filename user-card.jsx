import './user-card.scss'

export default function UserCard({
  avatar,
  firstname,
  lastname,
  email,
  phone,
}) {
  return (
    <div className="user">
      <img className="user__avatar" src={avatar} />
      <div className="user__info">
        <div className="user__info__name">
          {firstname} {lastname}
        </div>
        <br />
        Email: {email}
        <br />
        Phone: {phone}
        <br />
      </div>
    </div>
  )
}
