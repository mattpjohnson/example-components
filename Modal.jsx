import MicroModal from "https://unpkg.com/micromodal/dist/micromodal.min.js";
import "./Modal.scss";

export function Modal() {
  console.log({ MicroModal });

  setTimeout(() => MicroModal.init(), 1000);

  return (
    <div>
      <button
        onClick={function t() {
          MicroModal.show("#modal-1");
        }}
      >
        Open
      </button>
      <div id="modal-1" aria-hidden="true">
        <div tabindex="-1" data-micromodal-close>
          <div role="dialog" aria-modal="true" aria-labelledby="modal-1-title">
            <header>
              <h2 id="modal-1-title">Modal Title</h2>

              <button aria-label="Close modal" data-micromodal-close></button>
            </header>

            <div id="modal-1-content">Modal Content</div>
          </div>
        </div>
      </div>
    </div>
  );
}
