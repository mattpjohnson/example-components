import './text-field.scss'
import { Label } from './label.jsx'

export default function TextField({ children }) {
  return (
    <Label title={children}>
      <input className="va-text-field__input" />
    </Label>
  )
}
