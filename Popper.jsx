import Popper from "https://unpkg.com/popper.js";
import "./Popper.scss";

export function MyPopper({
  reference,
  popper,
  placement = "right",
  isopen = false
}) {
  popper = (
    <span className={`popper ${isopen ? "popper--is-open" : ""}`}>
      {popper}
    </span>
  );
  reference = <span>{reference}</span>;
  setTimeout(() => new Popper(reference, popper, { placement }), 0);

  return (
    <div>
      {reference}
      {popper}
    </div>
  );
}
