import 'https://cdn.jsdelivr.net/npm/pikaday/css/pikaday.css'

const Ul = ({ children }) => <ul>{children}</ul>
const Button = ({ children }) => <button>CoolButton {children}</button>

export default function ListView({ items }) {
  return (
    <Ul>
      {JSON.parse(items).map(item => (
        <li>
          Item: {item}{' '}
          <Button onclick={`alert('Hi! ${item}')`}>MyButton</Button>
        </li>
      ))}
    </Ul>
  )
}
